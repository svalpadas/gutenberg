package com.ignitsol.gutenberg.apiRequests;

import com.ignitsol.gutenberg.model.Books;

import java.util.List;
import java.util.Map;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;

public interface ApiService {

    @GET("books")
    Single<Books> getBooks(@QueryMap Map<String, String> queryBy);
    //query by mime_type, search = category & search = input user search
}
