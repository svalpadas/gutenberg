package com.ignitsol.gutenberg;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;

import com.ignitsol.gutenberg.adapter.CategoryAdapter;
import com.ignitsol.gutenberg.categorywise.CategoryBooksActivity;
import com.ignitsol.gutenberg.databinding.ActivityMainBinding;
import com.ignitsol.gutenberg.helper.Helper;
import com.ignitsol.gutenberg.interfaces.ItemClick;

public class MainActivity extends AppCompatActivity implements ItemClick {

    private ActivityMainBinding binding;
    private CategoryAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        binding = DataBindingUtil.setContentView(this,R.layout.activity_main);
        binding.executePendingBindings();
        binding.setLifecycleOwner(this);
        adapter = new CategoryAdapter(this, Helper.getCategory());//init adapter with category
        adapter.setItemClick(this);
        binding.setAdapter(adapter);

    }

    @Override
    public void onClick(View view, int index) {
        startActivity(new Intent(this, CategoryBooksActivity.class)
                .putExtra("CategoryName",adapter.getCategoryList().get(index).getName()));
    }
}
