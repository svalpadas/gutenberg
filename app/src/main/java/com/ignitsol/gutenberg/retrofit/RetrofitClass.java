package com.ignitsol.gutenberg.retrofit;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ignitsol.gutenberg.apiRequests.ApiService;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Wexoz on 29-09-2017.
 */

public class RetrofitClass {

    private static String token ;
    private static Gson gson = new GsonBuilder().setLenient().create();

    private static OkHttpClient client = new OkHttpClient.Builder().connectTimeout(30, TimeUnit.SECONDS).
            readTimeout(30, TimeUnit.SECONDS).writeTimeout(30, TimeUnit.SECONDS)
            .addInterceptor(new Interceptor() {
        @Override
        public Response intercept(Chain chain) throws IOException {
            Request newRequest  = chain.request().newBuilder()
                    .addHeader("Authorization", "Bearer " + token)
                    .build();
            return chain.proceed(newRequest);
        }
    }).build();

    public static String baseUrl = "http://skunkworks.ignitesol.com:8000/";
    public static ApiService getRetrofitUrl() {
        Retrofit retrofit = new Retrofit.Builder().client(client).baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create()).build();
        return retrofit.create(ApiService.class);
    }

}
