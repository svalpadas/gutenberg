package com.ignitsol.gutenberg.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.ignitsol.gutenberg.interfaces.ItemClick;
import com.ignitsol.gutenberg.R;
import com.ignitsol.gutenberg.databinding.IndividualBookBinding;
import com.ignitsol.gutenberg.model.Book;

import java.util.List;

public class BookAdapter extends RecyclerView.Adapter<BookAdapter.ViewHolder> {
    private Context context;
    private List<Book> bookList;

    public BookAdapter(Context context, List<Book> bookList) {
        this.context = context;
        this.bookList = bookList;
    }

    public List<Book> getBookList() {
        return bookList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.individual_book, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.binding.setBook(bookList.get(position));
        holder.binding.setIndex(position);
        if (position==(bookList.size()-1)&&itemClick!=null)
            itemClick.onClick(null,position);
    }

    @Override
    public int getItemCount() {
        return bookList !=null? bookList.size():0;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        IndividualBookBinding binding;

        ViewHolder(@NonNull IndividualBookBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            this.binding.setItemClick(itemClick);
        }

    }
    private ItemClick itemClick;
    public void setItemClick(ItemClick itemClick) {
        this.itemClick = itemClick;
    }//single item click
}
