package com.ignitsol.gutenberg.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.ignitsol.gutenberg.interfaces.ItemClick;
import com.ignitsol.gutenberg.R;
import com.ignitsol.gutenberg.databinding.IndividualCategoryBinding;
import com.ignitsol.gutenberg.model.Category;

import java.util.List;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.ViewHolder> {
    Context context;
    List<Category> categoryList;

    public CategoryAdapter(Context context, List<Category> categoryList) {
        this.context = context;
        this.categoryList = categoryList;
    }

    public List<Category> getCategoryList() {
        return categoryList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.individual_category, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.binding.setCategory(categoryList.get(position));
        holder.binding.setPosition(position);
    }

    @Override
    public int getItemCount() {
        return categoryList!=null?categoryList.size():0;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        IndividualCategoryBinding binding;

        ViewHolder(@NonNull IndividualCategoryBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            this.binding.setItemClick(itemClick);
        }

    }
    private ItemClick itemClick;
    public void setItemClick(ItemClick itemClick) {
        this.itemClick = itemClick;
    }//single item click
}
