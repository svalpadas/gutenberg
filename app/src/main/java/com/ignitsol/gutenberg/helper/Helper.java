package com.ignitsol.gutenberg.helper;

import com.ignitsol.gutenberg.R;
import com.ignitsol.gutenberg.model.Category;

import java.util.ArrayList;
import java.util.List;

public class Helper {


    private static List<Category> categoryList;

    public static List<Category> getCategory() {
        if (categoryList == null) {
            categoryList = new ArrayList<>();
            categoryList.add(new Category("FICTION", R.drawable.ic_fiction));
            categoryList.add(new Category("DRAMA",R.drawable.ic_drama));
            categoryList.add(new Category("HUMOR",R.drawable.ic_humour));
            categoryList.add(new Category("POLITICS",R.drawable.ic_politics));
            categoryList.add(new Category("PHILOSOPHY",R.drawable.ic_philosophy));
            categoryList.add(new Category("HISTORY",R.drawable.ic_history));
            categoryList.add(new Category("ADVENTURE",R.drawable.ic_adventure));
        }
        return categoryList;
    }
}
