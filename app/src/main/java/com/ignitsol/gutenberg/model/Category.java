package com.ignitsol.gutenberg.model;

import java.util.List;

public class Category {
    String name;
    int categoryResourceId;

    public Category(String name, int resourceId) {
        this.name = name;
        categoryResourceId = resourceId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCategoryResourceId() {
        return categoryResourceId;
    }

    public void setCategoryResourceId(int categoryResourceId) {
        this.categoryResourceId = categoryResourceId;
    }
}
