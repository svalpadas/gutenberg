package com.ignitsol.gutenberg.model;

import com.google.gson.annotations.SerializedName;

public class Formats {
    @SerializedName("application/pdf")
    String applicationPdf;
    @SerializedName("text/html")
    String textHtml;
    @SerializedName("text/plain")
    String textPlanTxt;
    @SerializedName("image/jpeg")
    String imageJpeg;

    public String getApplicationPdf() {
        return applicationPdf;
    }

    public void setApplicationPdf(String applicationPdf) {
        this.applicationPdf = applicationPdf;
    }

    public String getTextHtml() {
        return textHtml;
    }

    public void setTextHtml(String textHtml) {
        this.textHtml = textHtml;
    }

    public String getTextPlanTxt() {
        return textPlanTxt;
    }

    public void setTextPlanTxt(String textPlanTxt) {
        this.textPlanTxt = textPlanTxt;
    }

    public String getImageJpeg() {
        return imageJpeg;
    }

    public void setImageJpeg(String imageJpeg) {
        this.imageJpeg = imageJpeg;
    }
}
