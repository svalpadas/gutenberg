package com.ignitsol.gutenberg.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Books {
    private int count;
    private String next;
    private String previous = null;
    @SerializedName("results")
    private List<Book> books;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getNext() {
        return next;
    }

    public void setNext(String next) {
        this.next = next;
    }

    public String getPrevious() {
        return previous;
    }

    public void setPrevious(String previous) {
        this.previous = previous;
    }

    public List<Book> getBooks() {
        return books;
    }

    public void setBooks(List<Book> books) {
        this.books = books;
    }
}
