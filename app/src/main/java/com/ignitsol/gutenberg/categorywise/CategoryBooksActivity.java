package com.ignitsol.gutenberg.categorywise;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;

import com.google.android.material.textfield.TextInputLayout;
import com.ignitsol.gutenberg.R;
import com.ignitsol.gutenberg.databinding.ActivityCategoryBooksBinding;

public class CategoryBooksActivity extends AppCompatActivity implements TextWatcher, View.OnFocusChangeListener, View.OnClickListener {

    String categoryName;
    private ActivityCategoryBooksBinding binding;
    private CategoryViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_category_books);
        binding.executePendingBindings();
        binding.setLifecycleOwner(this);
        initView();
        viewModel = new ViewModelProvider(this).get(CategoryViewModel.class);
        if (getIntent() != null && getIntent().getStringExtra("CategoryName") != null) {
            categoryName = getIntent().getStringExtra("CategoryName");
        }
        viewModel.setCategory(categoryName);
        binding.setViewModel(viewModel);
        initActionBar();
    }

    private void initView() {
        binding.setFocused(false);
        binding.etSearchView.setOnFocusChangeListener(this);
        binding.etSearchView.addTextChangedListener(this);
        binding.ivCancel.setOnClickListener(this);
    }//

    private void initActionBar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(categoryName);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }//actionbar Initialization

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        if (s != null && s.getFilters().length > 0) {
            binding.setFocused(true);
            viewModel.searchBy(s.toString());
            binding.ivCancel.setVisibility(View.VISIBLE);
        } else {
            viewModel.searchBy(null);
            binding.ivCancel.setVisibility(View.GONE);
        }
    }//on text changed

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        binding.setFocused(hasFocus);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.ivCancel) {
            binding.etSearchView.setText("");
            binding.ivCancel.setVisibility(View.GONE);
        }//clear searched text
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
