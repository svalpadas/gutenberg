package com.ignitsol.gutenberg.categorywise;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.databinding.BindingAdapter;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.google.gson.JsonObject;
import com.ignitsol.gutenberg.interfaces.ItemClick;
import com.ignitsol.gutenberg.adapter.BookAdapter;
import com.ignitsol.gutenberg.apiRequests.ApiService;
import com.ignitsol.gutenberg.model.Book;
import com.ignitsol.gutenberg.model.Books;
import com.ignitsol.gutenberg.retrofit.RetrofitClass;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class CategoryViewModel extends AndroidViewModel implements ItemClick {
    private BookAdapter bookAdapter;
    private Context context;
    private String category;
    private ApiService apiService;
    private HashMap<String, String> filterBy;
    private CompositeDisposable compositeDisposable;
    private List<Book> books;
    private MutableLiveData<BookAdapter> bookAdapterMutableLiveData;
    private String prevUrl=null,nextUrl;
    private boolean isNext = false;
    public CategoryViewModel(@NonNull Application application) {
        super(application);
        context = application;
        filterBy = new HashMap<>();
        filterBy.put("mime_type", "image/jpeg");
        books = new ArrayList<>();
        apiService = RetrofitClass.getRetrofitUrl();
        bookAdapterMutableLiveData = new MutableLiveData<>();
        compositeDisposable = new CompositeDisposable();
        bookAdapter = new BookAdapter(context, books);
        bookAdapter.notifyDataSetChanged();
        bookAdapter.setItemClick(this);
    }

    @BindingAdapter({"bind:imageUrl"})
    public static void loadImage(ImageView view, String imageUrl) {
        Picasso.get()
                .load(imageUrl)
                .into(view);
    }

    public MutableLiveData<BookAdapter> getBookAdapterMutableLiveData() {
        return bookAdapterMutableLiveData;
    }

    public void setCategory(String category) {
        this.category = category;
        searchBy(null);
    }

    public void searchBy(String inputSearch) {
        filterBy = new HashMap<>();
        filterBy.put("mime_type", "image/jpeg");
        filterBy.put("search", category.toLowerCase());
        if (inputSearch != null && inputSearch.length() > 0) {
            isNext = false;
            filterBy.put("search", category.toLowerCase().concat(" ").concat(inputSearch));
        }
        compositeDisposable.clear();
        getBooks();
    }

    private void getBooks() {
        compositeDisposable.add(apiService.getBooks(filterBy)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<Books>() {
                    @Override
                    public void onSuccess(Books book) {
                        if (!isNext) {
                            books.clear();
                        }else {
                            isNext = false;
                            filterBy.remove("page");
                        }
                        if (book != null && book.getBooks() != null) {
                            nextUrl = book.getNext();
                            prevUrl = book.getPrevious();
                            books.addAll(book.getBooks());
                            Collections.sort(books);
                        }
                        bookAdapter.notifyDataSetChanged();
                        bookAdapterMutableLiveData.setValue(bookAdapter);
                    }

                    @Override
                    public void onError(Throwable e) {
                        Toast.makeText(context, e.toString(), Toast.LENGTH_SHORT).show();
                    }
                }));
    }//request API to get books

    @Override
    protected void onCleared() {
        super.onCleared();
        compositeDisposable.clear();
        compositeDisposable.dispose();
    }

    @Override
    public void onClick(View view, int index) {//browse file
        if (view!=null) {
            JsonObject object = books.get(index).getFormats();
            String htmlValue = null, pdfValue = null, txtValue = null;
            Uri htmlUri = null, pdfUri = null, txtUri = null;
            for (String value : object.keySet()) {
                if (value.contains("text/html") && object.get(value).getAsString().contains(".htm")) {
                    htmlUri = Uri.parse(object.get(value).getAsString());
                    htmlValue = value;
                    if (value.contains(";"))
                        htmlValue = value.split(";")[0];
                } else if (value.contains("application/pdf") && object.get(value).getAsString().contains(".pdf")) {
                    pdfUri = Uri.parse(object.get(value).getAsString());
                    pdfValue = value;
                    if (value.contains(";"))
                        pdfValue = value.split(";")[0];
                } else if (value.contains("text/plain") && object.get(value).getAsString().contains(".txt")) {
                    txtUri = Uri.parse(object.get(value).getAsString());
                    txtValue = value;
                    if (value.contains(";"))
                        txtValue = value.split(";")[0];
                }
            }
            if (htmlUri != null || pdfUri != null || txtUri != null) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setDataAndType(htmlUri != null ? htmlUri : pdfUri != null ? pdfUri : txtUri
                        , htmlValue != null ? htmlValue : pdfValue != null ? pdfValue : txtValue);//view as per sequence
                context.startActivity(intent);
            } else {
            }
        }//for book select view
        else{
            isNext = true;
            try {
                if (nextUrl!=null){
                    filterBy.put("page",""+nextUrl.split("&")[1].split("=")[1]);
                    getBooks();
                }
            }catch (Exception e){}
        }//load data from if available next url
    }
}
