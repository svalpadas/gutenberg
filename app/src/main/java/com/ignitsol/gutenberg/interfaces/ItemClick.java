package com.ignitsol.gutenberg.interfaces;

import android.view.View;

public interface ItemClick {
    void onClick(View view,int index);
}
